/*global angular  */

/* we 'inject' the ngRoute module into our app. This makes the routing functionality to be available to our app. */
var myApp = angular.module('myApp', ['ngRoute'])

/* the config function takes an array. */
myApp.config( ['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/search', {
		  templateUrl: 'templates/search.html',
      controller: 'searchController'
		})
    .when('/update', {
		  templateUrl: 'templates/update.html',
      controller: 'updateController'
		})
		.otherwise({
		  redirectTo: 'search'
		})
	}])


myApp.controller('searchController', function($scope, $http) {
  $scope.title = 'Enter a broadcaster here, then press Enter.'
  $scope.search = function($event) {
    console.log('search()')
    if ($event.which == 13) { // enter key presses
      var search = $scope.searchTerm
      console.log(search)
      var url = 'https://stream-info-davidsch-2.c9.io/broadcasters?broadcaster='+search
      $http.get(url).success(function(response) {
        console.log(response)
        $scope.message = response.message
        $scope.stream = response.data.stream
        $scope.game = response.data.game
        const stream = response.data.stream
        const game = response.data.game
       
        $scope.fps = Math.round(stream.fps)
        $scope.resolution = stream.resolution/9*16 + 'x' + stream.resolution
        $scope.creationDate = stream.channelCreated.substring(8, 10) + '/' + stream.channelCreated.substring(5, 7) + '/' + stream.channelCreated.substring(0, 4)
        $scope.onlineSince = stream.onlineSince.substring(11, 19) + ' UTC'
        
        if (stream.isTwitchPartner) {
            $scope.partner = 'Yes'
        } else {
            $scope.partner = 'No'
        }
        if (!stream.mature) {
            $scope.mature = 'No'
        } else {
            $scope.mature = 'Yes'
        }
        if (stream.streamAvailable) {
            $scope.streamAvailable = 'Yes'
        } else {
            $scope.streamAvailable = 'No'
        }
        if (game.gameAvailable) {
            $scope.gameAvailable = 'Yes'
        } else {
            $scope.gameAvailable = 'No'
        }
        
        $scope.searchTerm = ''
      })
    }
  }
})

myApp.controller('updateController', function($scope, $http) {
  $scope.title = 'Update a stream here:'
  $scope.update = function($event) {
    console.log('update()')
    if ($event.which == 13) { // enter key presses
      var user = $scope.user
      var summoner = $scope.summoner
      var region = $scope.region
      console.log(user)
      console.log(summoner)
      console.log(region)
      var url = 'https://stream-info-davidsch-2.c9.io/broadcasters'
      $http.put(url, { user: user, summoner: summoner, region: region }).success(function(response) {
          $scope.message = response.message
          console.log(response)
      })
    }
  }
})